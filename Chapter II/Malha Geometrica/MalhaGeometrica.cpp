//==============================================================================
// Name        : MalhaGeometrica.cpp
// Author      : Joao Flavio Vieira de Vasconcellos
// Version     : 1.0
// Description : Programa que calcula as coordenadas dos nos
//               para uma malha unidimensional de diferenças finitas
// Livro       : Codigo 2.1 do livro
//               Solução Numérica de Equações Diferenciais - 
//               O Método de Volumes Finitos
// Testado     : gcc 10.2 usando c++14 em 29 / set / 2020 
//
// Copyright   : Copyright (C) <2020>  Joao Flavio Vasconcellos 
//                                      (jflavio at iprj.uerj.br)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//==============================================================================


//==============================================================================
//      Includes c++
//==============================================================================

#include <algorithm>            // std::generate_n
#include <iomanip>              // std::setw
#include <iostream>             // std::cout , std::endl
#include <vector>               // std::vector

//==============================================================================
//      typedef
//==============================================================================

typedef     double                                   Real;
typedef     std::vector<Real>                        VetorReal;

//==============================================================================
//      Função para o cálculo de real elevado a um inteiro (mais rapido que pow)
//==============================================================================

constexpr Real ipow(    const Real& _base, 
                        const int& _exp, 
                        const Real& _result = 1) 
{
  return _exp < 1 ?  _result : 
                    ipow(   _base * _base, 
                            _exp >> 1, 
                            (_exp % 2) ? 
                            _result * _base : _result);
}

//==============================================================================
//      Função main
//==============================================================================

int main(int argc, char** argv) 
{

//==============================================================================
//      Definição das variáveis
//==============================================================================

const unsigned    NUMNODES(20);            // Número de nós da malha
const Real        LENGHT(1);               // Comprimento do domínio    
const Real        RATIO(1.09);             // Razão da progressão geométrica
const Real        XINIT(-1);               // Cordenada inicial    
    
Real                Dx;                    // Distância entre os nós
VetorReal           xNode(NUMNODES);       // Vetor com as coordenadas x dos nós

const unsigned      LSIZE(80);             // Para fins de impressão 

//==============================================================================
//      Impressão ddos dados iniciais
//==============================================================================

    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n";

    std::cout   << "\nGeracao de malhas de diferencas finitas usando progressao geometrica"
                << "\n\n";
    
    std::cout << std::fixed << std::setprecision(3) << std::right;
    
    std::cout   << "Numero de nos do dominio:         " 
                << std::setw(15) 
                << NUMNODES << std::endl;
    
    std::cout   << "Comprimento do dominio:           " 
                << std::setw(15) 
                << LENGHT << std::endl;
    
    std::cout   << "Coordenada inicial do dominio:    " 
                << std::setw(15) 
                << XINIT << std::endl;
    
    
//==============================================================================
//      Geração das coordenadas dos nós segundo uma progressão geométrica
//==============================================================================
    
    
    Dx = LENGHT * (RATIO - 1) / (ipow(RATIO, NUMNODES-1) - 1); // Cálculo do primeiro Delta x
    
    std::cout   << "Razao da progressao geometrica:   " 
                << std::setw(15) 
                << RATIO << std::endl;
    
    std::cout   << "Primeiro Delta x:                 " 
                << std::setw(15) 
                << std::scientific << std::setprecision(2)
                << Dx << "\n\n";
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n\n";
    

    Real x(XINIT);                    // Coordenada do primeiro nó
    
auto GeometricProgression = [&x, &Dx, RATIO] (auto&& _x)-> void {
    _x  = x;
    x += Dx;
    Dx *= RATIO;
   
};

    std::for_each   (   std::begin(xNode)
                    ,   std::end(xNode)
                    ,   GeometricProgression);

//==============================================================================
//      Impressão do resultado
//==============================================================================
            
    std::cout << std::scientific << std::setprecision(4) << std::right;

    std::cout << "Impressao das coordenadas dos nos da malha\n\n";
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n" 
              <<   std::setw(5)
              << "no"
              << std::setw(20)
              << "xNo"
              << "\n";
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n\n";
            
unsigned no(0);    
    
    
    for (auto xno : xNode) {
        std::cout << std::setw(5) << no++
                  << std::setw(20) << xno
                  << std::endl;
                
    }

    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << std::endl;
    
//==============================================================================
//      Apagando os vetores
//==============================================================================
    
    xNode.clear();
    
    return EXIT_SUCCESS;
    
}

