//==============================================================================
// Name        : RandomGrid.cpp
// Author      : Joao Flavio Vieira de Vasconcellos
// Version     : 1.0
// Description : Programa que calcula as coordenadas dos nos
//               para uma malha unidimensional de diferenças finitas
// Livro       : Codigo 2.2 do livro
//               Solução Numérica de Equações Diferenciais - 
//               O Método de Volumes Finitos
// Testado     : gcc 10.2 usando c++14 em 29 / set / 2020 
//
// Copyright   : Copyright (C) <2020>  Joao Flavio Vasconcellos 
//                                      (jflavio at iprj.uerj.br)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//==============================================================================


//==============================================================================
//      Includes c++
//==============================================================================

#include <algorithm>        // std::generate_n
#include <chrono>           // std::chrono::system_clock::now().time_since_epoch().count()
#include <iomanip>          // std::setw
#include <iostream>         // std::cout, std::endl
#include <random>           // std::default_random_engine 
#include <vector>           // std::vector


//==============================================================================
//      typedef
//==============================================================================

typedef double                                          Real;
typedef std::vector<Real>                               VecReal;        
typedef std::uniform_real_distribution<Real>            RandomDistribution;       

//==============================================================================
//      Função main
//==============================================================================


int main(int argc, char** argv) {

//==============================================================================
//      Definição das variáveis constamtes
//==============================================================================

    
const Real      XINIT(-4),               // Coordenada inicial da malha
                LENGHT(10);             // Comprimento da malha;

const int       NUMNODES(100);             // Número de volumes

const unsigned      LSIZE(80);          // Para fins de impressão 


//==============================================================================
//      Impressão ddos dados iniciais
//==============================================================================

    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n";

    std::cout   << "\nGeracao de malhas de diferencas finitas com nos distribuidos aleatoriamente"
                << "\n\n";
    
    std::cout << std::fixed << std::setprecision(3) << std::left;
    
    std::cout   << "Numero de nos do dominio:         " 
                << std::setw(15) 
                << NUMNODES << std::endl;
    
    std::cout   << "Comprimento do dominio:           " 
                << std::setw(15) 
                << LENGHT << std::endl;
    
    std::cout   << "Coordenada inicial do dominio:    " 
                << std::setw(15) 
                << XINIT << "\n\n";    
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n\n";
    

//==============================================================================
//      Definição das variáveis 
//==============================================================================


VecReal         xNode(NUMNODES);

    xNode[0] = XINIT;                       // coordenada do primeiro no     
    xNode[NUMNODES - 1] = XINIT + LENGHT;     // cordenada do ultimo no

//==============================================================================
//      Geração das coordenadas dos nós de forma aleatória
//==============================================================================

    
unsigned seed(std::chrono::system_clock::now().time_since_epoch().count());
std::default_random_engine             generator(seed);
RandomDistribution                     distribution (   xNode[0] 
                                                    ,   xNode[NUMNODES - 1]);

auto RandomNodes = [&] ()-> Real {
    return distribution(generator);
} ;  


    std::generate_n (   std::begin(xNode) + 1
                    ,   NUMNODES - 2
                    ,   RandomNodes);
    
//==============================================================================
//      Colocando todas as coordenadas em ordem crescente
//==============================================================================

                        
    std::sort   (   std::begin(xNode) + 1
                ,   std::end(xNode) - 1);              
    
//==============================================================================
//      Impressão do resultado
//==============================================================================

    std::cout << std::scientific << std::setprecision(4) << std::right;

    std::cout << "Impressao das coordenadas dos nos da malha\n\n";
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n" 
              <<   std::setw(5)
              << "no"
              << std::setw(20)
              << "xNo"
              << "\n";
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n\n";

            
unsigned no(0);    
    
    std::cout << std::scientific << std::setprecision(4);
    
    for (auto xno : xNode) {
        std::cout << std::setw(5) << no++
                  << std::setw(20) << xno
                  << std::endl;
                
    }
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << std::endl;
    
//==============================================================================
//      Apagando os vetores
//==============================================================================
    
    xNode.clear();
    
    return EXIT_SUCCESS;
    
}

