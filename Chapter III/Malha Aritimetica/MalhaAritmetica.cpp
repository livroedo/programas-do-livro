//==============================================================================
// Name        : MalhaAritimetica.cpp
// Author      : Joao Flavio Vieira de Vasconcellos
// Version     : 1.0
// Description : Programa que calcula as coordenadas dos nos
//               para uma malha unidimensional de volumes finitos.
//               A malha gerada é do tipo volume centrado
// Livro       : Codigo 3.1 do livro
//               Solução Numérica de Equações Diferenciais - 
//               O Método de Volumes Finitos
// Testado     : gcc 10.2 usando c++14 em 29 / set / 2020 
//
// Copyright   : Copyright (C) <2020>  Joao Flavio Vasconcellos 
//                                      (jflavio at iprj.uerj.br)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//==============================================================================




//==============================================================================
//      Includes c++
//==============================================================================

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <vector>

//==============================================================================
//      typedef
//==============================================================================

typedef     double                                   Real;
typedef     std::vector<Real>                        VetorReal;

//==============================================================================
//      Função main
//==============================================================================

int main(int argc, char** argv) 
{

//==============================================================================
//      Definição das  constantes do programa
//==============================================================================

const Real      XINIT(0),                  // Coordenada inicial da malha
                LENGHT(1);                 // Comprimento da malha;
const Real      RATIO(2e-04);              // Razão da progressão aritmética
const int       NVOLUMES(100);             // Número de volumes


const unsigned      LSIZE(80);              // Para fins de impressão 


//==============================================================================
//      Impressão ddos dados iniciais
//==============================================================================

    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n";

    std::cout   << "\nGeracao de malhas de volumes finitos utilizando progressao aritimetica\n"
                << "Malha tipo volume centrado"
                << "\n\n";
    
    std::cout << std::fixed << std::setprecision(3) << std::left;
    
    std::cout   << "Numero de nos do dominio:         " 
                << std::setw(15) 
                << NVOLUMES << std::endl;
    
    std::cout   << "Comprimento do dominio:           " 
                << std::setw(15) 
                << LENGHT << std::endl;
    
    std::cout   << "Coordenada inicial do dominio:    " 
                << std::setw(15) 
                << XINIT << "\n\n";   
    
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n\n";
    
    
//==============================================================================
//      Definição das variáveis
//==============================================================================

Real                    Dx0;                    // Distância entre os nós
VetorReal               xCentro(NVOLUMES);      // Vetor com as coordenadas do centro dos volumes
VetorReal               xFace(NVOLUMES+1);      // Vetor com as coordenadas do centro dos volumes    

    Dx0 = LENGHT / NVOLUMES - 0.5 * (NVOLUMES - 1) * RATIO;   // Cálculo do primeiro Delta x
    
    if (Dx0 <= 0) {
        
        Real ratio =  2 * LENGHT / ( (NVOLUMES - 1)  * NVOLUMES );
        std::cout   << "Problema na geracao da malha aritmetica\n"
                    << "Para esta malha a maior razao possivel eh:  " 
                    << std::scientific
                    << ratio
                    << "\nExecucao cancelada\n";
        abort();
                
    }
   
Real xF = XINIT;    
Real delta = 0;

    
auto ArithmeticProgression = [&xF, &delta, Dx0, RATIO] (auto&& _x)-> void {
    _x       = xF;
    xF      += Dx0 + delta;
    delta   += RATIO;
};

auto VolumeCentrado = [] (auto&& _xw, auto&& _xe) ->Real {
    return 0.5 * (_xe + _xw); 
};

    std::for_each   (   std::begin(xFace)
                    ,   std::end(xFace)
                    ,   ArithmeticProgression);

   
    std::transform  (   std::begin(xFace)
                    ,   std::end(xFace) - 1
                    ,   std::begin(xFace) + 1
                    ,   std::begin(xCentro)
                    ,   VolumeCentrado);
    
//==============================================================================
//      Impressão do resultado
//==============================================================================
            
    std::cout << std::scientific << std::setprecision(4) << std::right;

    std::cout << "Impressao das coordenadas dos nos da malha\n\n";
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n" 
              <<   std::setw(5)
              << "vol"
              << std::setw(20)
              << "xCentro"
              << std::setw(20)
              << "xFace"
              << "\n";
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n\n";
            
auto    xface   = std::begin(xFace);
auto    xcentro = std::begin(xCentro);
int     no(0);

    do {
        std::cout   << std::setw(5) << no
                    << std::setw(20) << *xcentro
                    << std::setw(20) << *xface
                    << std::endl;;
        
        ++no; ++xface;
    } while (++xcentro != std::end(xCentro));
    
    std::cout   << std::setw(5) << no
                << std::setw(40) << *xface
                << std::endl;;
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << std::endl;
                
//==============================================================================
//      Apagando os vetores
//==============================================================================
    

    xFace.clear();
    xCentro.clear();
    
//==============================================================================
//      Fim do Programa
//==============================================================================
    
    return EXIT_SUCCESS;
    
}

