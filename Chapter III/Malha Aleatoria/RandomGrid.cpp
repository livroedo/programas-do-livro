//==============================================================================
// Name        : RandomGrid.cpp
// Author      : Joao Flavio Vieira de Vasconcellos
// Version     : 1.0
// Description : Programa que calcula as coordenadas dos nos
//               para uma malha unidimensional de volumes finitos.
//               A malha gerada é do tipo volume centrado
// Livro       : Codigo 3.1 do livro
//               Solução Numérica de Equações Diferenciais - 
//               O Método de Volumes Finitos
// Testado     : gcc 10.2 usando c++14 em 29 / set / 2020 
//
// Copyright   : Copyright (C) <2020>  Joao Flavio Vasconcellos 
//                                      (jflavio at iprj.uerj.br)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//==============================================================================




//==============================================================================
//      Includes c++
//==============================================================================

#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <random>
#include <vector>


//==============================================================================
//      typedef
//==============================================================================

typedef double                                          Real;
typedef std::vector<Real>                               VecReal;        

//==============================================================================
//      Header das funções
//==============================================================================

void GeracaoFaceCentrada (VecReal&, VecReal&, const Real&, const Real&);
void GeracaoVolumeCentrado (VecReal&, VecReal&, const Real&, const Real&);
void CalculoDistancias (const VecReal&, const VecReal&, VecReal&, VecReal&);

//==============================================================================
//      Função main
//==============================================================================

int main(int argc, char** argv) {
    
//==============================================================================
//      Definição das  constantes do programa
//==============================================================================

enum    TipoGeracao {FACECENTRADA, VOLUMECENTRADO};             // Formas para a geração da malha
    
const Real      XINIT(2),                   // Coordenada inicial da malha
                LENGHT(1);                  // Comprimento da malha;
const int       NVOLUMES(20);               // Número de volumes


const unsigned      LSIZE(80);              // Para fins de impressão 


const TipoGeracao   tipoGeracao = VOLUMECENTRADO;


//==============================================================================
//      Impressão ddos dados iniciais
//==============================================================================

    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n";

    std::cout   << "\nGeracao de malhas de volumes finitos utilizando Malha Aleatória\n"
                << "Tipo de Malha: " 
                << (tipoGeracao == FACECENTRADA ? "Face Centrada ": "Volume Centrado")
                << "\n\n";
    
    std::cout << std::fixed << std::setprecision(3) << std::left;
    
    std::cout   << "Numero de nos do dominio:         " 
                << std::setw(15) 
                << NVOLUMES << std::endl;
    
    std::cout   << "Comprimento do dominio:           " 
                << std::setw(15) 
                << LENGHT << std::endl;
    
    std::cout   << "Coordenada inicial do dominio:    " 
                << std::setw(15) 
                << XINIT << "\n\n";   
    
    
    for (auto k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n\n";

//==============================================================================
//      Dimensionamento das variáveis
//==============================================================================
    
VecReal               xCentro(NVOLUMES);      // Vetor com as coordenadas do centro dos volumes
VecReal               xFace(NVOLUMES+1);      // Vetor com as coordenadas do centro dos volumes    

    switch(tipoGeracao) {
        case FACECENTRADA :     GeracaoFaceCentrada     (   xFace
                                                        ,   xCentro
                                                        ,   XINIT
                                                        ,   LENGHT
                                                        ); 
                                break;
                                
        case VOLUMECENTRADO :   GeracaoVolumeCentrado   (   xFace
                                                        ,   xCentro
                                                        ,   XINIT
                                                        ,   LENGHT
                                                        ); 
                                break;
    };


VecReal         dxFace(NVOLUMES);
VecReal         dxCentro(NVOLUMES+1);

    CalculoDistancias (xFace, xCentro, dxFace, dxCentro);
    
//==============================================================================
//      Impressão dos resultados finais
//==============================================================================

auto    ptrXFace = std::begin(xFace);
auto    ptrXCentro = std::begin(xCentro);
auto    ptrDXFace  = std::begin(dxFace);
auto    ptrDXCentro = std::begin(dxCentro);
int     index = 0;


    for (unsigned k=0; k< LSIZE; k++) std::cout << "=";
    std::cout << "\n";
    
    std::cout << std::setprecision(4) << std::fixed << std::right;
            
    std::cout << std::setw(6)
              << "vol"
              << std::setw(15) << "xFace"
              << std::setw(15) << "xCentro"
              << std::setw(15) << "dxFace"
              << std::setw(15) << "dxCentro"
              << "\n";
    
    
    do {
        
        std::cout << std::setw(6)   << index++
                  << std::setw(15)  << *ptrXFace 
                  << std::setw(15)  << *ptrXCentro 
                  << std::setw(15)  << *ptrDXFace 
                  << std::setw(15)  << *ptrDXCentro 
                  << std::endl;
        
        ++ptrXCentro; ++ptrDXFace; ++ptrDXCentro;
        
    } while (++ptrXFace != std::end(xFace) - 1);
    
    std::cout << std::setw(6)   << index
              << std::setw(15)  << *ptrXFace
              << std::setw(45)  << *ptrDXCentro 
              << std::endl;
    
    
//==============================================================================
//      Apagando os vetores
//==============================================================================
    
        xFace.clear();
        xCentro.clear();
        dxFace.clear();
        dxCentro.clear();
    
//==============================================================================
//      Fim do programa
//==============================================================================
    
    return EXIT_SUCCESS;
}

//==============================================================================
//      Função GeracaoFaceCentrada
//==============================================================================

void GeracaoFaceCentrada    (   VecReal&        _xFace
                            ,   VecReal&        _xCentro
                            ,   const Real&     _xIni
                            ,   const Real&     _lenght
                            ) 
{
    
//==============================================================================
//      Iniciando as variaveis para geracao aleatorio
//==============================================================================
    
unsigned seed(std::chrono::system_clock::now().time_since_epoch().count());
std::default_random_engine                       generator(seed);
std::uniform_real_distribution<Real>             distribution( _xIni, _xIni + _lenght);

auto RandomGenerator = [&] (auto&& _x)-> void {
    _x  = distribution(generator);
};

    std::for_each   (   std::begin(_xCentro)
                    ,   std::end(_xCentro)
                    ,   RandomGenerator);
    
    
    std::sort   (   std::begin(_xCentro) 
                ,   std::end(_xCentro)
                );    
    
auto ValorMedio = [] (auto&& _xw, auto&& _xe) ->Real {
    return 0.5 * (_xe + _xw); 
};    

    std::transform  (   std::begin(_xCentro)
                    ,   std::end(_xCentro) - 1
                    ,   std::begin(_xCentro) + 1
                    ,   std::begin(_xFace) + 1
                    ,   ValorMedio);
    
    *(std::begin(_xFace))   = _xIni;
    *(std::end(_xFace) - 1) = _xIni + _lenght;

}

//==============================================================================
//      Função GeracaoVolumeCentrado
//==============================================================================

void GeracaoVolumeCentrado  (   VecReal&        _xFace
                            ,   VecReal&        _xCentro
                            ,   const Real&     _xIni
                            ,   const Real&     _lenght
                            ) 
{
    
//==============================================================================
//      Iniciando as variaveis para geracao aleatorio
//==============================================================================
    
unsigned seed(std::chrono::system_clock::now().time_since_epoch().count());
std::default_random_engine                       generator(seed);
std::uniform_real_distribution<Real>             distribution( _xIni, _xIni + _lenght);
    
auto RandomGenerator = [&] (auto&& _x)-> void {
    _x  = distribution(generator);
};

    std::for_each   (   std::begin(_xFace) + 1
                    ,   std::end(_xFace) - 1
                    ,   RandomGenerator);
    
    *(std::begin(_xFace))   = _xIni;
    *(std::end(_xFace) - 1) = _xIni + _lenght;
    std::sort   (   std::begin(_xFace) + 1
                ,   std::end(_xFace) - 1
                );
    

auto ValorMedio = [] (auto&& _xw, auto&& _xe) ->Real {
    return 0.5 * (_xe + _xw); 
};    

    std::transform  (   std::begin(_xFace)
                    ,   std::end(_xFace) - 1
                    ,   std::begin(_xFace) + 1
                    ,   std::begin(_xCentro)
                    ,   ValorMedio);
    
    
    
}

//==============================================================================
//      Função CalculoDistancias
//      Calcula as distancias entre coordenadas das faces e entre as coordenadas 
//      dos centros dos volumes
//==============================================================================

void CalculoDistancias  (   const VecReal&          _xFace
                        ,   const VecReal&          _xCentro
                        ,   VecReal&                _dxFace
                        ,   VecReal&                _dxCentro
                        ) 
{
auto Distancia = [] (auto&& _xw, auto&& _xe) ->Real {
    return  (_xe - _xw); 
};

    std::transform  (   std::begin(_xFace)
                    ,   std::end(_xFace) - 1
                    ,   std::begin(_xFace) + 1
                    ,   std::begin(_dxFace)
                    ,   Distancia);    
    
    std::transform  (   std::begin(_xCentro)
                    ,   std::end(_xCentro) - 1
                    ,   std::begin(_xFace) + 1
                    ,   std::begin(_dxCentro) + 1
                    ,   Distancia);    
    
    
    *(std::begin(_dxCentro)) = *(std::begin(_xCentro)) - *(std::begin(_xFace));
    *(std::end(_dxCentro) - 1) = *(std::end(_xFace) - 1) - *(std::end(_xCentro) - 1);
    
};
